package main

import (
	"test-sevice-go/src/handler"
)

func main() {
	handler.LaunchServer()
}
