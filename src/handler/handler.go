package handler

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"strconv"
	"test-sevice-go/src/database"
	"test-sevice-go/src/service"
)

var s = service.PersonService{}

func LaunchServer() {
	log.Println("Launching server")
	db := database.GetDBConnection()
	if db == nil {
		log.Fatalf("ERROR: db connection not found")
	}
	log.Println("DB migration")
	db.AutoMigrate(&database.PersonTable{})

	http.HandleFunc("/add", addPerson)
	http.HandleFunc("/listAll", getAllPersons)
	http.HandleFunc("/getByName", getPersonsByName)
	http.HandleFunc("/getById", getPersonById)
	http.HandleFunc("/delete", deletePersonById)
	http.HandleFunc("/update", updatePerson)

	log.Println("Successfully started")
	http.ListenAndServe(":8080", nil)
}

func addPerson(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	jsonData, err := io.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = s.AddPerson(jsonData)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	responseJSON := []byte(`{"message": "Person added successfully"}`)
	w.Write(responseJSON)
}

func getAllPersons(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	pageStr := r.URL.Query().Get("page")
	page, _ := strconv.Atoi(pageStr)

	rsl := s.GetAllPersons(page)

	responseJSON, err := json.Marshal(rsl)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(responseJSON)
}

func getPersonById(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	name := r.URL.Query().Get("id")
	if name == "" {
		http.Error(w, "Missing 'id' parameter", http.StatusBadRequest)
		return
	}

	rsl := s.GetPersonByID(name)

	responseJSON, err := json.Marshal(rsl)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(responseJSON)
}

func getPersonsByName(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	name := r.URL.Query().Get("name")
	if name == "" {
		http.Error(w, "Missing 'name' parameter", http.StatusBadRequest)
		return
	}

	pageStr := r.URL.Query().Get("page")
	page, _ := strconv.Atoi(pageStr)

	rsl := s.GetPersonsByName(name, page)

	responseJSON, err := json.Marshal(rsl)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(responseJSON)
}

func deletePersonById(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodDelete {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	id := r.URL.Query().Get("id")
	if id == "" {
		http.Error(w, "Missing 'id' parameter", http.StatusBadRequest)
		return
	}

	err := s.DeletePersonById(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	responseJSON := []byte(`{"message": "Person deleted successfully"}`)
	w.Write(responseJSON)
}

func updatePerson(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPut {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	var updatedPerson database.PersonTable
	err := json.NewDecoder(r.Body).Decode(&updatedPerson)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = s.UpdatePerson(updatedPerson)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	responseJSON := []byte(`{"message": "Person updated successfully"}`)
	w.Write(responseJSON)
}
