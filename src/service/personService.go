package service

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"strconv"
	"test-sevice-go/src/database"
	enrichment "test-sevice-go/src/enricher"
)

type PersonService struct {
}

var db = database.GetDBConnection()

func (s *PersonService) EnrichPersonData(person *database.PersonTable) (*database.PersonTable, error) {
	enrichedPerson, err := enrichment.Enrich(person)
	if err != nil {
		return nil, err
	}

	return enrichedPerson, nil
}

func (s *PersonService) AddPerson(personData []byte) error {
	var person database.PersonTable

	err := json.Unmarshal(personData, &person)
	if err != nil {
		fmt.Println("Error unmarshalling JSON:", err)
		return err
	}

	enrichedPerson, err := s.EnrichPersonData(&person)
	if err == nil {
		err = database.AddPersonEntity(db, enrichedPerson)
		if err != nil {
			fmt.Println("AddPersonEntity error:", err)
		}
	}
	return nil
}

func (s *PersonService) GetAllPersons(page int) []database.PersonTable {
	return database.GetAllPersons(db, page)
}

func (s *PersonService) GetPersonsByName(name string, page int) []database.PersonTable {
	return database.GetPersonByName(db, name, page)
}

func (s *PersonService) GetPersonByID(personID string) database.PersonTable {
	return database.GetPersonByID(db, personID)
}

func (s *PersonService) DeletePersonById(id string) error {
	personID, err := strconv.ParseUint(id, 10, 64)
	if err != nil {
		return errors.New("invalid person ID")
	}

	err = database.DeletePersonByID(db, uint(personID))
	if err != nil {
		log.Printf("failed to delete person:%v", err)
		return err
	}

	return nil
}

func (s *PersonService) UpdatePerson(person database.PersonTable) error {
	err := database.UpdatePerson(db, &person)
	if err != nil {
		fmt.Println("Error updating person:", err)
		return err
	}

	return nil
}
