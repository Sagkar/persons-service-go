package database

import (
	"errors"
	"fmt"
	"gorm.io/gorm"
	"log"
)

type PersonTable struct {
	gorm.Model
	Name        string
	Surname     string
	Patronymic  string
	Age         string
	Gender      string
	Nationality string
}

func AddPersonEntity(db *gorm.DB, person *PersonTable) error {
	result := db.Create(person)

	if result.Error != nil {
		errString := fmt.Sprintf("person_table.AddPersonEntity action error:, %v", result.Error)
		log.Println(errString)
		return errors.New(errString)
	}

	log.Printf("Person %v added successfully", person)
	return nil
}

func GetAllPersons(db *gorm.DB, page int) []PersonTable {
	var persons []PersonTable
	pageSize := 10
	db.Order("id asc").Offset((page - 1) * pageSize).Limit(pageSize).Find(&persons)
	log.Printf("Getting list of all persons, records on page: %v, page:%v", len(persons), page)
	return persons
}

func GetPersonByName(db *gorm.DB, name string, page int) []PersonTable {
	var persons []PersonTable
	pageSize := 10
	db.Where("Name =?", name).Order("id asc").Offset((page - 1) * pageSize).Limit(pageSize).Find(&persons)
	log.Printf("Getting list of persons by name:%v", name)
	return persons
}

func GetPersonByID(db *gorm.DB, id string) PersonTable {
	var persons PersonTable
	db.Where("Id =?", id).Take(&persons)
	log.Printf("Getting person by ID:%v", id)
	return persons
}

func UpdatePerson(db *gorm.DB, person *PersonTable) error {
	result := db.Updates(person)
	if result.Error != nil {
		return result.Error
	}

	log.Printf("Person %v updated successfully", person)
	return nil
}

func DeletePersonByID(db *gorm.DB, id uint) error {
	result := db.Delete(&PersonTable{Model: gorm.Model{ID: id}})
	if result.Error != nil {
		return result.Error
	}

	log.Printf("Person with ID %d deleted successfully", id)
	return nil
}
