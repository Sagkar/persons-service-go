package enrichment

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"
	"test-sevice-go/src/database"
)

type AgifyResp struct {
	Name string `json:"name"`
	Age  int    `json:"age"`
}

type GenderizeResp struct {
	Name   string `json:"name"`
	Gender string `json:"gender"`
}

type NationalizeResp struct {
	Name    string `json:"name"`
	Country []struct {
		CountryId   string  `json:"country_id"`
		Probability float64 `json:"probability"`
	} `json:"country"`
}

func Enrich(person *database.PersonTable) (*database.PersonTable, error) {
	name := person.Name

	agifyResponse, err := getAge(name)
	if err != nil {
		log.Fatalf("enricher.getAge fail: %v", err)
	}
	person.Age = strconv.Itoa(agifyResponse.Age)

	genderizeResponse, err := getGender(name)
	if err != nil {
		log.Fatalf("enricher.getGender fail: %v", err)
	}
	person.Gender = genderizeResponse.Gender

	nationalizeResponse, err := getNationality(name)
	if err != nil {
		log.Fatalf("enricher.getGender fail: %v", err)
	}

	if len(nationalizeResponse.Country) > 0 {
		person.Nationality = nationalizeResponse.Country[0].CountryId
	}

	log.Println("Enriching entity...")
	return person, nil
}

func getAge(name string) (*AgifyResp, error) {
	response, err := http.Get(fmt.Sprintf("https://api.agify.io/?name=%s", name))
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	body, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	var agify AgifyResp
	err = json.Unmarshal(body, &agify)
	if err != nil {
		return nil, err
	}

	return &agify, nil
}

func getGender(name string) (*GenderizeResp, error) {
	response, err := http.Get(fmt.Sprintf("https://api.genderize.io/?name=%s", name))
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	body, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	var genderize GenderizeResp
	err = json.Unmarshal(body, &genderize)
	if err != nil {
		return nil, err
	}

	return &genderize, nil
}

func getNationality(name string) (*NationalizeResp, error) {
	response, err := http.Get(fmt.Sprintf("https://api.nationalize.io/?name=%s", name))
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	body, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	var nationalize NationalizeResp
	err = json.Unmarshal(body, &nationalize)
	if err != nil {
		return nil, err
	}

	return &nationalize, nil
}
