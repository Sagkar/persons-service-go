# persons-service-go

# Persons Service
Проект представляет собой реализацию сервиса для работы с людьми. API позволяет получать, добавлять, изменять и удалять людей из базы данных PostgreSQL.
При добавлении новой записи, её поля обогащаются из открытых API.

# Требования
- Golang 1.17+
- PostgreSQL

# Структура проекта
- src/database: файлы для подключения к базе данных и работы с таблицей людей
- src/enricher: функционал для обогащения записей в базе данных наиболее вероятными возрастом, полом и национальностью.
- src/service: сервис для работы с базой данных
- src/handler: функции для обработки HTTP-запросов к API сервиса.

# Использование

Для добавления новой записи, отправьте POST-запрос на эндпоинт `/add`, передав в теле запроса json вида:
{
"name": "Dmitriy",
"surname": "Ushakov",
"patronymic": "Vasilevich" // необязательно
}

Пример:
```bash
curl -X POST -H "Content-Type: application/json" -d '{"name": "Dmitriy", "surname": "Ushakov", "patronymic": "Vasilevich", "age": 25, "gender": "male", "nationality": "RU"}' http://localhost:8080/add
```

Для постраничного получение всех записей, отправьте GET-запрос:
```bash
curl http://localhost:8080/listAll?page=1
```

Для получение записи по ID, отправьте GET-запрос:
```bash
curl http://localhost:8080/getById?id=10
````

Для получение записей по имени, отправьте GET-запрос:
```bash
curl http://localhost:8080/getByName?name=Dmitry&page=1
```

Для удаления записи по ID DELETE-запрос:
```bash
curl -X DELETE http://localhost:8080/delete?id=1
```

Для обновление записи:

```bash
curl -X PUT -H "Content-Type: application/json" -d '{"ID": 1, "Name": "UpdatedName", "Surname": "UpdatedSurname", "Patronymic": "UpdatedPatronymic", "Age": 30, "Gender": "male", "Nationality": "US"}' http://localhost:8080/update
```